﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AISCinemaApp2.Models.ViewModels
{
    public class FilmAddViewModel
    {
        public string name { get; set; }
        public decimal rating { get; set; }
        public int duration { get; set; }
        public int idGenre { get; set; }
        public DateTime releaseDate { get; set; }
        public string description { get; set; }
        public HttpPostedFileBase posterImage { get; set; }
        public string pathToImage { get; set; }
        public bool isActive { get; set; }

        public static Film ConvertToFilm(FilmAddViewModel vm)
        {
            return new Film()
            {
                id = 0,
                name = vm.name,
                duration = vm.duration,
                idGenre = vm.idGenre,
                releaseDate = vm.releaseDate,
                description = vm.description,
                pathToImage = vm.pathToImage,
                rating = vm.rating,
                isActive = vm.isActive
            };
        }
    }
}