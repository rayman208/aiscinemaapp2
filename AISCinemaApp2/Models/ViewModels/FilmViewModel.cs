﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AISCinemaApp2.Models.ViewModels
{
    public class FilmViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal rating { get; set; }
        public int duration { get; set; }
        public string genre { get; set; }
        public DateTime releaseDate { get; set; }
        public string description { get; set; }
        public string pathToImage { get; set; }

        static public List<FilmViewModel> ConvertMany(List<Film> films)
        {
            List<FilmViewModel> convertFilms = new List<FilmViewModel>();

            foreach (Film film in films)
            {
                convertFilms.Add(new FilmViewModel()
                {
                    id = film.id,
                    name = film.name,
                    description = film.description,
                    duration = film.duration,
                    genre = film.Genre.name,
                    rating = film.rating,
                    releaseDate = film.releaseDate,
                    pathToImage = film.pathToImage
                }
                );
            }

            return convertFilms;
        }

        static public FilmViewModel ConvertOne(Film film)
        {
            return new FilmViewModel()
            {
                id = film.id,
                name = film.name,
                description = film.description,
                duration = film.duration,
                genre = film.Genre.name,
                rating = film.rating,
                releaseDate = film.releaseDate,
                pathToImage = film.pathToImage
            };
        }
    }
}