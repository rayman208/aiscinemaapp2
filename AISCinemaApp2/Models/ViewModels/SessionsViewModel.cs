﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AISCinemaApp2.Models.ViewModels
{
    public class SessionsViewModel
    {
        public Film film { get; set; }
        public List<SessionViewModel> sessions { get; set; }
    }
}