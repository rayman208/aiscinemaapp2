﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AISCinemaApp2.Models.ViewModels
{
    public class GenreViewModel
    {
        public int id { get; set; }
        public string name { get; set; }

        static public List<GenreViewModel> ConvertMany(List<Genre> genres)
        {
            List<GenreViewModel> convertGenres = new List<GenreViewModel>();

            foreach (Genre genre in genres)
            {
                convertGenres.Add(new GenreViewModel()
                {
                    id = genre.id,
                    name = genre.name
                });
            }

            return convertGenres;
        }
    }
}