﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AISCinemaApp2.Models.ViewModels
{
    public class SessionViewModel
    {
        public int idSession { get; set; }
        public DateTime dt { get; set; }
        public int price { get; set; }
        public int countTickets { get; set; }

        static public List<SessionViewModel> ConvertMany(List<Session> sessions)
        {
            List<SessionViewModel> convertSessions = new List<SessionViewModel>();

            foreach (Session session in sessions)
            {
                convertSessions.Add(new SessionViewModel()
                {
                    idSession = session.id,
                    dt = session.dt,
                    price = session.price,
                    countTickets = session.countTickets
                }
                );
            }

            return convertSessions;
        }
    }
}