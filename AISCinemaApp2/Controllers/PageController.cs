﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AISCinemaApp2.Models;
using AISCinemaApp2.Models.ViewModels;
using System.IO;

namespace AISCinemaApp2.Controllers
{
    public class PageController : Controller
    {
        private AISCinemaEntities db;

        public PageController()
        {
            db = new AISCinemaEntities();
        }

        public ActionResult Index()
        {
            List<FilmViewModel> films = FilmViewModel.ConvertMany(db.Films.Where(item => item.isActive == true).ToList());
            return View(films);
        }

        public ActionResult Details(int? id)
        {
            ReturnedResultAndViewError<bool> checkResult = CheckValueIdFilm(id);
            if (checkResult.Value == false)
            {
                return checkResult.ViewError;
            }

            ReturnedResultAndViewError<Film> findResult = FindFilmById(id.Value);
            if (findResult.Value == null)
            {
                return findResult.ViewError;
            }

            FilmViewModel film = FilmViewModel.ConvertOne(findResult.Value);

            return View(film);
        }

        public ActionResult TableSessions(int? id)
        {
            ReturnedResultAndViewError<bool> checkResult = CheckValueIdFilm(id);
            if (checkResult.Value == false)
            {
                return checkResult.ViewError;
            }

            ReturnedResultAndViewError<Film> findResult = FindFilmById(id.Value);
            if (findResult.Value == null)
            {
                return findResult.ViewError;
            }

            SessionsViewModel sessions = new SessionsViewModel()
            {
                film = findResult.Value,
                sessions = SessionViewModel.ConvertMany(db.Sessions.Where(item => item.idFilm == findResult.Value.id).ToList())
            };

            return View(sessions);
        }

        [HttpPost]
        public ActionResult BuyTicket(int? idFilm, int? idSession)
        {
            ReturnedResultAndViewError<bool> checkIdFilmResult = CheckValueIdFilm(idFilm);
            if (checkIdFilmResult.Value == false)
            {
                return checkIdFilmResult.ViewError;
            }

            ReturnedResultAndViewError<bool> checkidSessionResult = CheckValueIdSession(idSession);
            if (checkidSessionResult.Value == false)
            {
                return checkidSessionResult.ViewError;
            }

            ReturnedResultAndViewError<Film> findFilmResult = FindFilmById(idFilm.Value);
            if (findFilmResult.Value == null)
            {
                return findFilmResult.ViewError;
            }

            ReturnedResultAndViewError<Session> findSessionResult = FindSessionById(idSession.Value);
            if (findSessionResult.Value == null)
            {
                return findSessionResult.ViewError;
            }

            ReturnedResultAndViewError<bool> checkIsActiveFilmResult = CheckIsActiveFilm(findFilmResult.Value);
            if (checkIsActiveFilmResult.Value == false)
            {
                return checkIsActiveFilmResult.ViewError;
            }

            db.Tickets.Add(new Ticket()
            {
                idFilm = idFilm.Value,
                idSession = idSession.Value,
                idUser = 3
            });

            findSessionResult.Value.countTickets--;

            db.SaveChanges();

            SuccessViewModel success = new SuccessViewModel()
            {
                message = "Билет успешно приобретён"
            };

            return View("CommonSuccess", success);
        }

        public ActionResult AddFilmForm()
        {
            List<GenreViewModel> genres = GenreViewModel.ConvertMany(db.Genres.ToList());
            return View(genres);
        }

        [HttpPost]
        public ActionResult AddFilm(FilmAddViewModel filmAddViewModel)
        {
            try
            {
                int unixTimestamp = (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                string imageExt = Path.GetExtension(filmAddViewModel.posterImage.FileName);
                string savePath = Server.MapPath("~/Content/Images/") + unixTimestamp + imageExt;

                string dbPath = "/Content/Images/" + unixTimestamp + imageExt;

                filmAddViewModel.posterImage.SaveAs(savePath);

                filmAddViewModel.pathToImage = dbPath;
                filmAddViewModel.isActive = true;

                Film film = FilmAddViewModel.ConvertToFilm(filmAddViewModel);

                db.Films.Add(film);
                db.SaveChanges();

                SuccessViewModel success = new SuccessViewModel()
                {
                    message = "Фильм успешно добавлен!"
                };

                return View("CommonSuccess", success);
            }
            catch
            {
                ErrorViewModel error = new ErrorViewModel()
                {
                    message = "Ошибка придобавлении фильма"
                };
                return View("CommonError", error);
            }

        }
        #region Helpers

        private ReturnedResultAndViewError<bool> CheckValueIdFilm(int? idFilm)
        {
            if (idFilm.HasValue == false)
            {
                ErrorViewModel error = new ErrorViewModel()
                {
                    message = "ID фильма не задан"
                };

                return new ReturnedResultAndViewError<bool>(false, View("CommonError", error));
            }
            else
            {
                return new ReturnedResultAndViewError<bool>(true, null);
            }
        }

        private ReturnedResultAndViewError<bool> CheckIsActiveFilm(Film film)
        {
            if (film.isActive == false)
            {
                ErrorViewModel error = new ErrorViewModel()
                {
                    message = "Фильм не активен"
                };

                return new ReturnedResultAndViewError<bool>(false, View("CommonError", error));
            }
            else
            {
                return new ReturnedResultAndViewError<bool>(true, null);
            }
        }

        private ReturnedResultAndViewError<bool> CheckValueIdSession(int? idSession)
        {
            if (idSession.HasValue == false)
            {
                ErrorViewModel error = new ErrorViewModel()
                {
                    message = "ID сесанса не задан"
                };

                return new ReturnedResultAndViewError<bool>(false, View("CommonError", error));
            }
            else
            {
                return new ReturnedResultAndViewError<bool>(true, null);
            }
        }

        private ReturnedResultAndViewError<Film> FindFilmById(int idFilm)
        {
            Film findedFilm = db.Films.FirstOrDefault(item => item.id == idFilm);

            if (findedFilm == null)
            {
                ErrorViewModel error = new ErrorViewModel()
                {
                    message = "Фильм с заданным ID не найден"
                };

                return new ReturnedResultAndViewError<Film>(null, View("CommonError", error));
            }
            else
            {
                return new ReturnedResultAndViewError<Film>(findedFilm, null);
            }
        }

        private ReturnedResultAndViewError<Session> FindSessionById(int idSession)
        {
            Session findedSession = db.Sessions.FirstOrDefault(item => item.id == idSession);

            if (findedSession == null)
            {
                ErrorViewModel error = new ErrorViewModel()
                {
                    message = "Сеанс с заданным ID не найден"
                };

                return new ReturnedResultAndViewError<Session>(null, View("CommonError", error));
            }
            else
            {
                return new ReturnedResultAndViewError<Session>(findedSession, null);
            }
        }


        #endregion
    }
}