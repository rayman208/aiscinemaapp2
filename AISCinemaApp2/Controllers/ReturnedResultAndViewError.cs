﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AISCinemaApp2.Controllers
{
    public class ReturnedResultAndViewError<T>
    {
        public T Value { get; private set; }
        public ViewResult ViewError { get; private set; }

        public ReturnedResultAndViewError(T Value, ViewResult ViewError)
        {
            this.Value = Value;
            this.ViewError = ViewError;
        }
    }
}